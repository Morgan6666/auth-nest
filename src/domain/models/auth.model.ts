import { IEntity } from "domain/shared/entity.interface";



export class AuthModel implements IEntity {
    id?: number;
    password?: string;
    

    constructor(
        id?: number,
        password?: string
    ) {
        this.id = id;
        this.password = password;
    }

    equals(entity: IEntity): boolean {
        if (!(entity instanceof AuthModel)) return false;
        return this.id === entity.id;
      }


}