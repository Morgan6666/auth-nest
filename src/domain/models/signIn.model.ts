import { IEntity } from "domain/shared/entity.interface";



export class SignInModel implements IEntity {
    id: string;
    password: string;

    constructor(
        id: string,
        password: string
    ) {
        this.id = id;
        this.password = password;
    }
    equals(entity: IEntity): boolean {
        if (!(entity instanceof SignInModel)) return false;
        return this.id === entity.id;
      }
}