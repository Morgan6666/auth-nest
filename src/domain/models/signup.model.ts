import { IEntity } from "domain/shared/entity.interface";



export class SignUpModel implements IEntity {
    password: string;

    constructor(
        password: string
    ) {
        this.password = password;
    }
    equals(entity: IEntity): boolean {
        if (!(entity instanceof SignUpModel)) return false;
        return this.password === entity.password;
      }
}
