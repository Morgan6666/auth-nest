
import { Module } from "@nestjs/common";

import { JwtModule } from "@nestjs/jwt";
import { MongooseModule } from "@nestjs/mongoose";
import { Auth, AuthSchema } from "infrastructure/schemas/auth.schema";
import { AuthContoroller } from "presentation/controllers/auth.controller";
import { JWTUtil } from "infrastructure/utils/jwt.utils";
import { JwtStrategy } from "infrastructure/strategies/jwt.strategy";
import { jwtConstants } from "infrastructure/utils/secret.const";
import { AuthUseCase } from "application/use-cases/auth.usecase";


@Module({
    imports:[
        
    MongooseModule.forRoot("mongodb://localhost:27017"),
    MongooseModule.forFeature([
      { name: Auth.name, schema: AuthSchema },
      
    ]),
        JwtModule.register({
        global: true,
        secret: jwtConstants.secret,
        signOptions: { expiresIn: '60s' },
      }),],
    controllers: [AuthContoroller],
    providers: [
        AuthUseCase,
        JWTUtil,
        JwtStrategy
        

    ]
})
export class AuthModule {}