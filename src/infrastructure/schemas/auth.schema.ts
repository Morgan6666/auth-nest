import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";


export type AuthDocument = Auth & Document;

@Schema({timestamps: true})
export class Auth {
   
    
    @Prop({required: true})
    password: string;
}

export const AuthSchema = SchemaFactory.createForClass(Auth);