import { Injectable, Logger } from "@nestjs/common";

import * as bcrypt from "bcrypt";

import { InjectModel } from "@nestjs/mongoose";
import { Auth, AuthDocument } from "infrastructure/schemas/auth.schema";
import { Model } from "mongoose";

import { JwtService } from "@nestjs/jwt";
import { SignUpModel } from "domain/models/signup.model";
import { SignInModel } from "domain/models/signIn.model";
import { JwtPayload } from "infrastructure/interface/jwt.interface";
import { ServiceResponse } from "infrastructure/utils/service.res";
import { salt } from "infrastructure/utils/secret.const";
import {
  ISignInRes,
  ISignUpRes,
} from "infrastructure/interface/sign.interface";
import { IBaseRes } from "infrastructure/interface/base.interface";
import { IUserRes } from "infrastructure/interface/user.interface";

@Injectable()
export class AuthUseCase {
  private readonly logger = new Logger(AuthUseCase.name);
  public serviceRes = new ServiceResponse();

  constructor(
    private jwtService: JwtService,
    @InjectModel(Auth.name) private authModel: Model<AuthDocument>
  ) {}

  async signUp(data: SignUpModel): Promise<ISignUpRes | IBaseRes> {
    this.logger.log(`Хэшируем пароль`);
    data.password = await bcrypt.hash(data.password, salt);
    this.logger.log("Сохраняем данные в коллекцию");
    const result = await this.authModel.create(data);
    if (result) {
      this.logger.log(`Запись успешно создана`);
      const token: string = this.jwtService.sign({ id: result._id });
      return this.serviceRes.uniqueSuccessRes({ id: result._id, token: token });
    } else {
      this.logger.log(`Ошибка добавления в mongodb`);
      return this.serviceRes.internalServerError();
    }
  }

  async signIn(data: SignInModel): Promise<ISignInRes | IBaseRes> {
    this.logger.log(`Проверяем наличие записи в mongodb`);
    const result = await this.authModel.findById({ _id: data.id });
    if (result) {
      this.logger.log(`Проверяем пароль`);
      const isMatch = await bcrypt.compare(data.password, result.password);
      if (isMatch) {
        this.logger.log(`Генерируем токен`);
        const token: string = this.jwtService.sign({ id: result._id });
        return this.serviceRes.uniqueSuccessRes({ token: token });
      } else {
        this.logger.log(`Пароли не совадают`);
        return this.serviceRes.badIdOrPassword();
      }
    } else {
      this.logger.log(`Запись не найдена`);
      return this.serviceRes.badIdOrPassword();
    }
  }

  async getUser(id: string): Promise<IUserRes | IBaseRes> {
    this.logger.log(`Проверяем наличие записи в mongodb`);
    const result = await this.authModel.findById({ _id: id });
    if (result) {
      this.logger.log(`Запись существует`);
      return this.serviceRes.uniqueSuccessRes({ id: id });
    } else {
      this.logger.log(`Запись не существует`);
      return this.serviceRes.recordDoesntExist();
    }
  }

  async verifyPayload(payload: JwtPayload): Promise<JwtPayload | IBaseRes> {
    this.logger.log(`Проверяем payload`);
    const result = await this.authModel.findById({ _id: payload.id });
    if (result) {
      this.logger.log("id существует");
      return payload;
    } else {
      this.logger.log("id не сущетсвует");
      return this.serviceRes.badToken();
    }
  }
}
