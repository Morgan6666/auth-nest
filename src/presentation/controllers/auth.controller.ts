import {
  Body,
  Controller,
  Get,
  Post,
  UseGuards,
} from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { ApiTags } from "@nestjs/swagger";
import { Headers } from "@nestjs/common";
import { JWTUtil } from "infrastructure/utils/jwt.utils";
import { SignUpVM } from "presentation/view-model/signup.vm";
import { SignInVM } from "presentation/view-model/signin.vm";
import { AuthUseCase } from "application/use-cases/auth.usecase";
@ApiTags("Auth")
@Controller("auth")
export class AuthContoroller {
  constructor(
    private readonly jwtUtil: JWTUtil,
    private readonly authUseCase: AuthUseCase
  ) {}

  @Post("signup")
  async signup(@Body() data: SignUpVM) {
    const result = await this.authUseCase.signUp(SignUpVM.fromViewModel(data));
    return result;
  }

  @Post("signin")
  async signin(@Body() data: SignInVM) {
    const result = await this.authUseCase.signIn(SignInVM.fromViewModel(data));
    return result;
  }

  @Get("user/")
  @UseGuards(AuthGuard("jwt"))
  async getUser(@Headers("Authorization") auth: string) {
    const data = await this.jwtUtil.decode(auth);
    
    const result = await this.authUseCase.getUser(data['id']);
    return result;
  }
}
