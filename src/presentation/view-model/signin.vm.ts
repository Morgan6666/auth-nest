import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";
import { SignInModel } from "domain/models/signIn.model";



export class SignInVM {
    
    @IsString()
    @IsNotEmpty()
    @ApiProperty({
        description: "Collection id",
        example: "6478fe60cdfe5fcf303bb2d9"
    })
    id: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty({
        description: "User password",
        example: "test5"
    })
    password: string

    static fromViewModel(vm: SignInVM): SignInModel {
        return new SignInModel(vm.id, vm.password)

    }
}