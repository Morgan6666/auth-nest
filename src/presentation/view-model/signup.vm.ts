import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";
import { SignUpModel } from "domain/models/signup.model";
import e from "express";



export class SignUpVM {
    

    @IsString()
    @IsNotEmpty()
    @ApiProperty({
        description: "User password",
        example: "test5"
    })
    password: string;

    static fromViewModel(vm: SignUpVM): SignUpModel {
        return new SignUpModel(vm.password)

    }
}