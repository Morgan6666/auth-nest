
## Описание
  #API запросы
  ```bash
    /auth/signin
  URL: http://localhost:3011/auth/signin
  Method: POST
  Payload: 
    {
    "id": "6478fe60cdfe5fcf303bb2d9",
    "password": "test5"
    }

  Res: 
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0NzhmZTYwY2RmZTVmY2YzMDNiYjJkOSIsImlhdCI6MTY4NTY3ODMyNCwiZXhwIjoxNjg1Njc4Mzg0fQ.tqH4hXubD_hTvv_T-4Syc0Qa35jK9TeTyJ4KFpJl1po"
    }

    /auth/signup
  URL: http://localhost:3011/auth/signup
  Method: POST
  Payload: 
    {
    "password": "test5"
    }
  Res: 
    "id": "6479506fae3169b17f9c66bf",
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.     eyJpZCI6IjY0Nzk1MDZmYWUzMTY5YjE3ZjljNjZiZiIsImlhdCI6MTY4NTY3MjA0NywiZXhwIjoxNjg1NjcyMTA3fQ.B5id9M9uOnpRN8BSWxbFcqIOC_r1DwqhuvMQGrILAUQ"
    }


    /auth/user
  URL: http://localhost:3011/auth/user
  Method: GET
  Auth: Barier



## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash


## Support

- Author - Denis Bairamkulow
- Telegram - @DenisDeonis
- Email - dbairamkulow@mail.ru



