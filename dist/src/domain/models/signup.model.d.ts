import { IEntity } from "domain/shared/entity.interface";
export declare class SignUpModel implements IEntity {
    password: string;
    constructor(password: string);
    equals(entity: IEntity): boolean;
}
