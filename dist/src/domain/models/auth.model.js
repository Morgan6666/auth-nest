"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthModel = void 0;
class AuthModel {
    constructor(id, password) {
        this.id = id;
        this.password = password;
    }
    equals(entity) {
        if (!(entity instanceof AuthModel))
            return false;
        return this.id === entity.id;
    }
}
exports.AuthModel = AuthModel;
//# sourceMappingURL=auth.model.js.map