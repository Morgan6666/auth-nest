import { IEntity } from "domain/shared/entity.interface";
export declare class SignInModel implements IEntity {
    id: string;
    password: string;
    constructor(id: string, password: string);
    equals(entity: IEntity): boolean;
}
