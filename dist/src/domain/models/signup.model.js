"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SignUpModel = void 0;
class SignUpModel {
    constructor(password) {
        this.password = password;
    }
    equals(entity) {
        if (!(entity instanceof SignUpModel))
            return false;
        return this.password === entity.password;
    }
}
exports.SignUpModel = SignUpModel;
//# sourceMappingURL=signup.model.js.map