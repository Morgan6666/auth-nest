"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SignInModel = void 0;
class SignInModel {
    constructor(id, password) {
        this.id = id;
        this.password = password;
    }
    equals(entity) {
        if (!(entity instanceof SignInModel))
            return false;
        return this.id === entity.id;
    }
}
exports.SignInModel = SignInModel;
//# sourceMappingURL=signIn.model.js.map