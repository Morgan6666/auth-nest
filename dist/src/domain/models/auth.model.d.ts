import { IEntity } from "domain/shared/entity.interface";
export declare class AuthModel implements IEntity {
    id?: number;
    password?: string;
    constructor(id?: number, password?: string);
    equals(entity: IEntity): boolean;
}
