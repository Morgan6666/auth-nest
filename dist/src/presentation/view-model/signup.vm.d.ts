import { SignUpModel } from "domain/models/signup.model";
export declare class SignUpVM {
    password: string;
    static fromViewModel(vm: SignUpVM): SignUpModel;
}
