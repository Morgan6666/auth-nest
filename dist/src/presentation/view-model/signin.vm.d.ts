import { SignInModel } from "domain/models/signIn.model";
export declare class SignInVM {
    id: string;
    password: string;
    static fromViewModel(vm: SignInVM): SignInModel;
}
