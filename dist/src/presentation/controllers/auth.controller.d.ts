import { JWTUtil } from "infrastructure/utils/jwt.utils";
import { SignUpVM } from "presentation/view-model/signup.vm";
import { SignInVM } from "presentation/view-model/signin.vm";
import { AuthUseCase } from "application/use-cases/auth.usecase";
export declare class AuthContoroller {
    private readonly jwtUtil;
    private readonly authUseCase;
    constructor(jwtUtil: JWTUtil, authUseCase: AuthUseCase);
    signup(data: SignUpVM): Promise<import("../../infrastructure/interface/base.interface").IBaseRes | import("../../infrastructure/interface/sign.interface").ISignUpRes>;
    signin(data: SignInVM): Promise<import("../../infrastructure/interface/base.interface").IBaseRes | import("../../infrastructure/interface/sign.interface").ISignInRes>;
    getUser(auth: string): Promise<import("../../infrastructure/interface/base.interface").IBaseRes | import("../../infrastructure/interface/user.interface").IUserRes>;
}
