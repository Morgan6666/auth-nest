"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthContoroller = void 0;
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const swagger_1 = require("@nestjs/swagger");
const common_2 = require("@nestjs/common");
const jwt_utils_1 = require("../../infrastructure/utils/jwt.utils");
const signup_vm_1 = require("../view-model/signup.vm");
const signin_vm_1 = require("../view-model/signin.vm");
const auth_usecase_1 = require("../../application/use-cases/auth.usecase");
let AuthContoroller = class AuthContoroller {
    constructor(jwtUtil, authUseCase) {
        this.jwtUtil = jwtUtil;
        this.authUseCase = authUseCase;
    }
    async signup(data) {
        const result = await this.authUseCase.signUp(signup_vm_1.SignUpVM.fromViewModel(data));
        return result;
    }
    async signin(data) {
        const result = await this.authUseCase.signIn(signin_vm_1.SignInVM.fromViewModel(data));
        return result;
    }
    async getUser(auth) {
        const data = await this.jwtUtil.decode(auth);
        const result = await this.authUseCase.getUser(data['id']);
        return result;
    }
};
__decorate([
    (0, common_1.Post)("signup"),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [signup_vm_1.SignUpVM]),
    __metadata("design:returntype", Promise)
], AuthContoroller.prototype, "signup", null);
__decorate([
    (0, common_1.Post)("signin"),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [signin_vm_1.SignInVM]),
    __metadata("design:returntype", Promise)
], AuthContoroller.prototype, "signin", null);
__decorate([
    (0, common_1.Get)("user/"),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)("jwt")),
    __param(0, (0, common_2.Headers)("Authorization")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AuthContoroller.prototype, "getUser", null);
AuthContoroller = __decorate([
    (0, swagger_1.ApiTags)("Auth"),
    (0, common_1.Controller)("auth"),
    __metadata("design:paramtypes", [jwt_utils_1.JWTUtil,
        auth_usecase_1.AuthUseCase])
], AuthContoroller);
exports.AuthContoroller = AuthContoroller;
//# sourceMappingURL=auth.controller.js.map