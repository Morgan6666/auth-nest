"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var AuthUseCase_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthUseCase = void 0;
const common_1 = require("@nestjs/common");
const bcrypt = __importStar(require("bcrypt"));
const mongoose_1 = require("@nestjs/mongoose");
const auth_schema_1 = require("../../infrastructure/schemas/auth.schema");
const mongoose_2 = require("mongoose");
const jwt_1 = require("@nestjs/jwt");
const service_res_1 = require("../../infrastructure/utils/service.res");
const secret_const_1 = require("../../infrastructure/utils/secret.const");
let AuthUseCase = AuthUseCase_1 = class AuthUseCase {
    constructor(jwtService, authModel) {
        this.jwtService = jwtService;
        this.authModel = authModel;
        this.logger = new common_1.Logger(AuthUseCase_1.name);
        this.serviceRes = new service_res_1.ServiceResponse();
    }
    async signUp(data) {
        this.logger.log(`Хэшируем пароль`);
        data.password = await bcrypt.hash(data.password, secret_const_1.salt);
        this.logger.log("Сохраняем данные в коллекцию");
        const result = await this.authModel.create(data);
        if (result) {
            this.logger.log(`Запись успешно создана`);
            const token = this.jwtService.sign({ id: result._id });
            return this.serviceRes.uniqueSuccessRes({ id: result._id, token: token });
        }
        else {
            this.logger.log(`Ошибка добавления в mongodb`);
            return this.serviceRes.internalServerError();
        }
    }
    async signIn(data) {
        this.logger.log(`Проверяем наличие записи в mongodb`);
        const result = await this.authModel.findById({ _id: data.id });
        if (result) {
            this.logger.log(`Проверяем пароль`);
            const isMatch = await bcrypt.compare(data.password, result.password);
            if (isMatch) {
                this.logger.log(`Генерируем токен`);
                const token = this.jwtService.sign({ id: result._id });
                return this.serviceRes.uniqueSuccessRes({ token: token });
            }
            else {
                this.logger.log(`Пароли не совадают`);
                return this.serviceRes.badIdOrPassword();
            }
        }
        else {
            this.logger.log(`Запись не найдена`);
            return this.serviceRes.badIdOrPassword();
        }
    }
    async getUser(id) {
        this.logger.log(`Проверяем наличие записи в mongodb`);
        const result = await this.authModel.findById({ _id: id });
        if (result) {
            this.logger.log(`Запись существует`);
            return this.serviceRes.uniqueSuccessRes({ id: id });
        }
        else {
            this.logger.log(`Запись не существует`);
            return this.serviceRes.recordDoesntExist();
        }
    }
    async verifyPayload(payload) {
        this.logger.log(`Проверяем payload`);
        const result = await this.authModel.findById({ _id: payload.id });
        if (result) {
            this.logger.log("id существует");
            return payload;
        }
        else {
            this.logger.log("id не сущетсвует");
            return this.serviceRes.badToken();
        }
    }
};
AuthUseCase = AuthUseCase_1 = __decorate([
    (0, common_1.Injectable)(),
    __param(1, (0, mongoose_1.InjectModel)(auth_schema_1.Auth.name)),
    __metadata("design:paramtypes", [jwt_1.JwtService,
        mongoose_2.Model])
], AuthUseCase);
exports.AuthUseCase = AuthUseCase;
//# sourceMappingURL=auth.usecase.js.map