import { AuthDocument } from "infrastructure/schemas/auth.schema";
import { Model } from "mongoose";
import { JwtService } from "@nestjs/jwt";
import { SignUpModel } from "domain/models/signup.model";
import { SignInModel } from "domain/models/signIn.model";
import { JwtPayload } from "infrastructure/interface/jwt.interface";
import { ServiceResponse } from "infrastructure/utils/service.res";
import { ISignInRes, ISignUpRes } from "infrastructure/interface/sign.interface";
import { IBaseRes } from "infrastructure/interface/base.interface";
import { IUserRes } from "infrastructure/interface/user.interface";
export declare class AuthUseCase {
    private jwtService;
    private authModel;
    private readonly logger;
    serviceRes: ServiceResponse;
    constructor(jwtService: JwtService, authModel: Model<AuthDocument>);
    signUp(data: SignUpModel): Promise<ISignUpRes | IBaseRes>;
    signIn(data: SignInModel): Promise<ISignInRes | IBaseRes>;
    getUser(id: string): Promise<IUserRes | IBaseRes>;
    verifyPayload(payload: JwtPayload): Promise<JwtPayload | IBaseRes>;
}
