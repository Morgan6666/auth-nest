"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthModule = void 0;
const common_1 = require("@nestjs/common");
const jwt_1 = require("@nestjs/jwt");
const mongoose_1 = require("@nestjs/mongoose");
const auth_schema_1 = require("../schemas/auth.schema");
const auth_controller_1 = require("../../presentation/controllers/auth.controller");
const jwt_utils_1 = require("../utils/jwt.utils");
const jwt_strategy_1 = require("../strategies/jwt.strategy");
const secret_const_1 = require("../utils/secret.const");
const auth_usecase_1 = require("../../application/use-cases/auth.usecase");
let AuthModule = class AuthModule {
};
AuthModule = __decorate([
    (0, common_1.Module)({
        imports: [
            mongoose_1.MongooseModule.forRoot("mongodb://localhost:27017"),
            mongoose_1.MongooseModule.forFeature([
                { name: auth_schema_1.Auth.name, schema: auth_schema_1.AuthSchema },
            ]),
            jwt_1.JwtModule.register({
                global: true,
                secret: secret_const_1.jwtConstants.secret,
                signOptions: { expiresIn: '60s' },
            }),
        ],
        controllers: [auth_controller_1.AuthContoroller],
        providers: [
            auth_usecase_1.AuthUseCase,
            jwt_utils_1.JWTUtil,
            jwt_strategy_1.JwtStrategy
        ]
    })
], AuthModule);
exports.AuthModule = AuthModule;
//# sourceMappingURL=auth.module.js.map