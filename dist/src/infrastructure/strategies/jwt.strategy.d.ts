import { JwtPayload } from "infrastructure/interface/jwt.interface";
import { AuthUseCase } from "application/use-cases/auth.usecase";
declare const JwtStrategy_base: new (...args: any[]) => any;
export declare class JwtStrategy extends JwtStrategy_base {
    private readonly authService;
    constructor(authService: AuthUseCase);
    validate(payload: JwtPayload): Promise<JwtPayload | import("../interface/base.interface").IBaseRes>;
}
export {};
