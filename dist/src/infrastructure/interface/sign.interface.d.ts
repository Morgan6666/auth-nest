import { IBaseRes } from "./base.interface";
export interface ISignUpRes extends IBaseRes {
    content: {
        id: number;
        token: string;
    };
}
export interface ISignInRes extends IBaseRes {
    content: {
        token: string;
    };
}
